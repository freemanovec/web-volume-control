#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;

use web_volume_control::audio::AudioManager;

use rocket::response::content::{
    Html,
    Json
};

#[get("/")]
fn handler_index() -> Html<String> {
    Html(include_str!("../../index.html").to_string())
}

struct AdjustmentResponse {
    pub volume: f32
}

#[get("/?<adjust>")]
fn handler_adjust(adjust: f32) -> Json<String> {
    let audio_manager = AudioManager::new().unwrap();
    let new_volume = audio_manager.adjust_volume(adjust).unwrap();
    audio_manager.cleanup();
    let formatted_output = format!("{{\"volume\": {}}}", new_volume);
    Json(formatted_output)
}

fn main() {
    rocket::ignite().mount("/", routes![
        handler_index,
        handler_adjust
    ]).launch();
    /*let audio_manager = AudioManager::new().unwrap();
    audio_manager.adjust_volume(0.01).unwrap();
    audio_manager.cleanup();*/
}