use std::{
    ffi::OsStr,
    iter::once,
    ptr::null_mut
};
#[cfg(target_family = "windows")]
use winapi::{
    shared::{
        winerror::SUCCEEDED,
        wtypesbase::CLSCTX_INPROC_SERVER
    },
    um::{
        mmdeviceapi::{
            IMMDeviceEnumerator,
            MMDeviceEnumerator,
            IMMDevice
        },
        endpointvolume::IAudioEndpointVolume,
        combaseapi::{
            CoCreateInstance,
            CLSCTX_ALL,
            CoUninitialize
        },
        objbase::CoInitialize
    },
};

#[cfg(target_family = "windows")]
pub struct AudioManager<'a> {
    endpoint: &'a IAudioEndpointVolume
}

#[cfg(not(target_family = "windows"))]
pub struct AudioManager;

#[cfg(target_family = "windows")]
impl<'a> AudioManager<'a> {

    pub fn new() -> Result<Self, String> {
        unsafe {
            let hr = CoInitialize(null_mut());
            if !SUCCEEDED(hr) {
                return Err(format!("CoInitialize failed (0x{:X})", hr));
            }
    
            let mut device_enumerator: *mut winapi::ctypes::c_void = null_mut();
            let hr = CoCreateInstance(
                &<MMDeviceEnumerator as winapi::Class>::uuidof(),
                null_mut(),
                CLSCTX_ALL,
                &<IMMDeviceEnumerator as winapi::Interface>::uuidof(),
                &mut device_enumerator
            );
            if !SUCCEEDED(hr) {
                return Err(format!("CoCreateInstance failed (0x{:X})", hr));
            }
            let device_enumerator: &IMMDeviceEnumerator = &*(device_enumerator as *mut IMMDeviceEnumerator);
            let mut default_audio_endpoint: *mut IMMDevice = null_mut();
            let hr = device_enumerator.GetDefaultAudioEndpoint(
                winapi::um::mmdeviceapi::eRender,
                winapi::um::mmdeviceapi::eMultimedia,
                &mut default_audio_endpoint
            );
            if !SUCCEEDED(hr) {
                return Err(format!("GetDefaultAudioEndpoint failed (0x{:X})", hr));
            }
            let default_audio_endpoint: &IMMDevice = &*(default_audio_endpoint as *mut IMMDevice);
            let mut endpoint_volume: *mut winapi::ctypes::c_void = null_mut();  
            let hr = default_audio_endpoint.Activate(
                &<IAudioEndpointVolume as winapi::Interface>::uuidof(),
                CLSCTX_INPROC_SERVER,
                null_mut(),
                &mut endpoint_volume
            );
            if !SUCCEEDED(hr) {
                return Err(format!("Activate failed (0x{:X})", hr));
            }
            let endpoint_volume: &IAudioEndpointVolume = &*(endpoint_volume as *mut IAudioEndpointVolume);
            return Ok(Self {
                endpoint: endpoint_volume
            });
        }
    }

    pub fn get_volume(&self) -> Result<f32, String> {
        unsafe {
            let mut current_volume: f32 = 0.0;
            let hr = self.endpoint.GetMasterVolumeLevelScalar(
        &mut current_volume
            );
            if !SUCCEEDED(hr) {
                return Err(format!("GetMasterVolumeLevelScalar failed (0x{:X})", hr));
            }
            return Ok(current_volume);
        }
    }

    fn set_volume(&self, level: f32) -> Result<(), String> {
        unsafe {
            let hr = self.endpoint.SetMasterVolumeLevelScalar(
                level,
                null_mut()
            );
            if !SUCCEEDED(hr) {
                return Err(format!("SetMasterVolumeLevelScalar failed (0x{:X})", hr));
            }
            return Ok(());
        }
    }

    pub fn adjust_volume(&self, adjustment: f32) -> Result<f32, String> {
        let volume = self.get_volume();
        if volume.is_ok() {
            let volume = volume.unwrap();
            let volume = volume + adjustment;
            let volume = if volume < 0.0 {
                0.0
            } else if volume > 1.0 {
                1.0
            } else {
                volume
            };
            if let Err(error) = self.set_volume(volume) {
                Err(error)
            } else {
                Ok(volume)
            }
        } else {
            let error = volume.unwrap_err();
            Err(error)
        }
    }

    pub fn cleanup(self) {
        unsafe {
            self.endpoint.Release();
            CoUninitialize();
        }
    }

}

#[cfg(not(target_family = "windows"))]
impl AudioManager {
    pub fn new() -> Result<Self, String> {
        println!("[MOCK] Initializing new AudioManager");
        Ok(Self {})
    }

    pub fn get_volume(&self) -> Result<f32, String> {
        println!("[MOCK] Returning dummy volume level");
        Ok(0.42)
    }

    fn set_volume(&self, level: f32) -> Result<(), String> {
        println!("[MOCK] Dummy setting volume to {}", level);
        Ok(())
    }

    pub fn adjust_volume(&self, adjustment: f32) -> Result<f32, String> {
        let initial = self.get_volume().unwrap();
        let target = initial + adjustment;
        println!("[MOCK] Dummy adjusting base volume {} by {} to {}", initial, adjustment, target);
        Ok(target)
    }

    pub fn cleanup(self) {
    }
}